package vcl

import (
	"reflect"

	"gitee.com/ying32/govcl/vcl/api"
	"gitee.com/ying32/govcl/vcl/rtl"
	. "gitee.com/ying32/govcl/vcl/types"
)

// ShowMessage 显示一个消息框
func ShowMessage(msg string) {
	api.DShowMessage(msg)
}

// MessageDlg 消息框，Buttons为按钮样式，祥见types.TMsgDlgButtons
func MessageDlg(Msg string, DlgType TMsgDlgType, Buttons ...uint8) int32 {
	return api.DMessageDlg(Msg, DlgType, TMsgDlgButtons(rtl.Include(0, Buttons...)), 0)
}

// CheckPtr 检测接口是否被实例化，如果已经实例化则返回实例指针
func CheckPtr(value IObject) uintptr {
	if value == nil || reflect.ValueOf(value).Pointer() == 0 {
		return 0
	}
	return value.Instance()
}
